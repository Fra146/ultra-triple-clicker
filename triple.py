import time
from pynput.mouse import Button, Controller, Listener

mouse = Controller()

def triple_click():
	time.sleep(0.1)
	mouse.click(Button.left)
	time.sleep(0.1)
	mouse.click(Button.left)
	time.sleep(0.1)
	main()

def on_click(x, y, button, pressed):
	if pressed == True:
		triple_click()

def main():
	with Listener(on_click=on_click) as listener:
		listener.join()

if __name__ == "__main__":
	main()